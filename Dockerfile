FROM gcr.io/kaniko-project/executor:v1.8.1-debug

ARG YQ_VERSION=4.27.5

RUN \
    mkdir -p /usr/local/bin; \
    kubectl_version=$(wget --no-check-certificate https://storage.googleapis.com/kubernetes-release/release/stable.txt -q -O -) && \
    wget --no-check-certificate -q -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$kubectl_version/bin/linux/amd64/kubectl; \
    \
    wget --no-check-certificate -q -O /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64; \
    wget --no-check-certificate -q -O /usr/local/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64; \
    \
    chmod +x /usr/local/bin/*
